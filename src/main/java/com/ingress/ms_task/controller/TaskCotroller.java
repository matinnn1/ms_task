package com.ingress.ms_task.controller;

import com.ingress.ms_task.dto.request.TaskRequestDto;
import com.ingress.ms_task.dto.response.TaskResponseDto;
import com.ingress.ms_task.entity.Task;
import com.ingress.ms_task.service.inter.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/task")
@RequiredArgsConstructor
public class TaskCotroller {
    private final TaskService taskService;
    @GetMapping()
    public List<TaskResponseDto> getAll(){
        return taskService.findAll();
    }

    @PostMapping()
    public TaskResponseDto add(@RequestBody TaskRequestDto taskRequestDto){
        return taskService.addMainTask(taskRequestDto);
    }

    @PostMapping("/{mainTaskId}")
    public TaskResponseDto add(@PathVariable Long mainTaskId,@RequestBody TaskRequestDto taskRequestDto) throws Exception{
          return taskService.addSubTask(mainTaskId,taskRequestDto);
    }

    @PutMapping("/{mainTaskId}")
    public TaskResponseDto update(@PathVariable Long mainTaskId,@RequestBody TaskRequestDto taskRequestDto) throws Exception{
        return taskService.update(mainTaskId,taskRequestDto);
    }
}