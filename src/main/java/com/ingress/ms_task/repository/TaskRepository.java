package com.ingress.ms_task.repository;

import com.ingress.ms_task.entity.Task;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    @Override
    @EntityGraph(attributePaths = {"subTasks"}, type = EntityGraph.EntityGraphType.FETCH)
    List<Task> findAll();
}
