package com.ingress.ms_task.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;
    private LocalDateTime dedline;
    private boolean active;

    @OneToMany(mappedBy = "mainTask", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Task> subTasks = new ArrayList<>() ;

    @ManyToOne
    @JoinColumn(name = "main_task_id")
    @JsonIgnore
    private Task mainTask;
}
