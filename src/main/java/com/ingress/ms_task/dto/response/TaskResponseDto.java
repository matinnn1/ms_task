package com.ingress.ms_task.dto.response;

import com.ingress.ms_task.entity.Task;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TaskResponseDto {
    private Long id;
    private String name;
    private String description;
    private LocalDateTime dedline;
    private boolean active;
    private List<TaskResponseDto> subTasks = new ArrayList<>();
}
