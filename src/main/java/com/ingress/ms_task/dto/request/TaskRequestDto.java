package com.ingress.ms_task.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ingress.ms_task.entity.Task;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TaskRequestDto {
    private String name;
    private String description;
    private LocalDateTime dedline;
    private boolean active;
}