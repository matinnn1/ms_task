package com.ingress.ms_task.service.impl;

import com.ingress.ms_task.dto.request.TaskRequestDto;
import com.ingress.ms_task.dto.response.TaskResponseDto;
import com.ingress.ms_task.entity.Task;
import com.ingress.ms_task.mapper.TaskMapper;
import com.ingress.ms_task.repository.TaskRepository;
import com.ingress.ms_task.service.inter.TaskService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class TaskManager implements TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;

    @Override
    public List<TaskResponseDto> findAll() {
       List<Task> tasksFromDb =  taskRepository.findAll();

       return taskMapper.listEntityToResponseDto(tasksFromDb);
    }

    @Override
    public TaskResponseDto addMainTask(TaskRequestDto taskRequestDto) {
        Task task = taskMapper.requestDtoToEntity(taskRequestDto);
        Task createdTask = taskRepository.save(task);

        return taskMapper.entityToResponseDto(createdTask);
    }

    @Override
    public TaskResponseDto addSubTask(Long mainTaskId, TaskRequestDto taskRequestDto) throws RuntimeException {
        Task mainTask = taskRepository.findById(mainTaskId).orElseThrow(()->new RuntimeException("Task not found!"));

        if(!mainTask.isActive())
            throw new RuntimeException("Task is not active");

        Task task = taskMapper.requestDtoToEntity(taskRequestDto);

        task.setMainTask(mainTask);

        Task createdTask = taskRepository.save(task);

        return taskMapper.entityToResponseDto(createdTask);
    }

    @Override
    public TaskResponseDto update(Long taskId, TaskRequestDto taskRequestDto) {
         Task mainTaskFromDb = taskRepository.findById(taskId).orElseThrow(()->new RuntimeException("Task not found!"));

         List<Task> subTasks =  new ArrayList<>();

         for (Task subTask : mainTaskFromDb.getSubTasks()){
             subTask.setActive(taskRequestDto.isActive());
             subTasks.add(subTask);
         }

         mainTaskFromDb = taskMapper.requestDtoToEntity(taskRequestDto);
         mainTaskFromDb.setId(taskId);

         mainTaskFromDb.setSubTasks(subTasks);;

         Task updatedTask = taskRepository.save(mainTaskFromDb);
         return  taskMapper.entityToResponseDto(updatedTask);
    }
}
