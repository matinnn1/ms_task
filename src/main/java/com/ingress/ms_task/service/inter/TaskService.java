package com.ingress.ms_task.service.inter;

import com.ingress.ms_task.dto.request.TaskRequestDto;
import com.ingress.ms_task.dto.response.TaskResponseDto;
import com.ingress.ms_task.entity.Task;

import java.util.List;

public interface TaskService {
    List<TaskResponseDto> findAll();
    TaskResponseDto addMainTask(TaskRequestDto taskRequestDto);
    TaskResponseDto addSubTask(Long mainTaskId,TaskRequestDto taskRequestDto) throws Exception;
    TaskResponseDto update(Long taskId,TaskRequestDto taskRequestDto);
}
