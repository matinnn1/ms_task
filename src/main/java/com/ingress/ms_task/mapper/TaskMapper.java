package com.ingress.ms_task.mapper;

import com.ingress.ms_task.dto.request.TaskRequestDto;
import com.ingress.ms_task.dto.response.TaskResponseDto;
import com.ingress.ms_task.entity.Task;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)

public interface TaskMapper {


    List<TaskResponseDto> listEntityToResponseDto(List<Task> tasks);
    TaskResponseDto entityToResponseDto(Task tasks);

    List<Task> listResponseDtoToEntity(List<TaskResponseDto> taskResponseDtoList);

    TaskRequestDto entityToRequestDto(Task task);
    Task requestDtoToEntity(TaskRequestDto taskRequestDto);

}
